import React from 'react'
import '../../css/app.css'
export default function FooterBanner() {
    return (
        <React.Fragment>
            {/* WEB FOOTER  */}
            <div className='h-auto hidden  lg:grid grid-cols-8 bg-new'>
                <div className='h-auto col-span-5 relative order-1 '>
                    <div className="ml-32  mb-[90px] mt-[60px] flex items-start">
                        <div className='w-[15%] order-1 mr-4'>
                            <img src='https://d3sgbq9gctgf5o.cloudfront.net/footers/erha-ultimate.png?format=auto'></img>
                        </div>
                        <div className='w-[50%] order-2 mr-3'>
                            <h4 className='text-[#F58220] text-start text-[20px] font-bold'>Hubungi Kami</h4>
                            <p className='text-white text-[14px] font-bold text-start'>
                                DISTRICT 8 Building Treasury Tower
                                <br />
                                36th & 37th Floor SCBD Lot 28
                                <br />
                                Jl. Jend Sudirman Kav.52 - 53 Jakarta 12190
                            </p>
                            <p className='text-white text-[14px] font-bold text-start flex items-start justify-start mt-4 gap-2'>
                                <img className='w-6 ' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/icon-7.png?format=auto'></img>
                                <a className='hover:text-[#F58220]' href='mailto:dear@erha.co.id'>dear@erha.co.id</a>
                            </p>
                            <p className='text-white text-[14px] font-bold text-start flex items-start justify-start mt-4 gap-2'>
                                <img className='w-6 ' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/icon-5.png?format=auto'></img>
                                <a className='hover:text-[#F58220]' href='tel:02150922121' >(021) 5092 2121</a>
                            </p>
                            <p className='text-white text-[14px] font-bold text-start flex items-start justify-start mt-4 gap-2'>
                                <img className='w-6 ' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/wa-logo.png?format=auto'></img>
                                <a className='hover:text-[#F58220]' href='https://wa.me/6281121212121' target='_blank' >+62-811-2121-2121</a>
                            </p>
                        </div>
                        <div className='mr-14 w-auto order-3'>
                            <h4 className='text-[#F58220] text-star text-[20px] font-bold'>Ikuti Kami</h4>
                            <div className='mt-2'>
                                <ul className='flex gap-1 items-center justify-start'>
                                    <li>
                                        <a href='https://www.facebook.com/erhadermatology.official' target='_blank' >
                                            <img className='px-[7px] py-[10px] w-12' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/facebook.png?format=auto'></img>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='https://www.instagram.com/erha.ultimate/' target='_blank'>
                                            <img className='px-[7px] py-[10px] w-12' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/instagram.png?format=auto'></img>
                                        </a>

                                    </li>
                                    <li>
                                        <a href='https://www.youtube.com/@dearerha' target='_blank'>
                                            <img className='px-[7px] py-[10px] w-12' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/youtube.png?format=auto'></img>
                                        </a>

                                    </li>
                                    <li>
                                        <a href='https://www.tiktok.com/@erha.ultimate' target='_blank'>
                                            <img className='px-[7px] py-[10px] w-12' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/tiktok.png?format=auto'></img>
                                        </a>

                                    </li>
                                </ul>
                            </div>
                            <h4 className='text-[#F58220] text-star text-[20px] font-bold mt-3'>Kebijakan Privasi</h4>
                        </div>
                    </div>
                </div>
                <div className='h-auto col-span-3 ml-6 relative order-2 '>
                    <div className='mr-28 mb-[90px] mt-[60px] flex'>
                        <img className='z-10 w-[60%]' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/footer-phone.png?format=auto'></img>
                        <div className='px-2 py-5 flex flex-col gap-2'>
                            <h4 className='text-[#101820] text-[1.25vw] font-bold'>ERHA Buddy</h4>
                            <h5 className='text-white font-semibold mb-7 pt-1 text-[1.25vw]'>Download&nbsp;Sekarang</h5>
                            <img className='mr-4 w-36' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/playstore.png?format=auto'></img>
                            <img className='mr-4 w-36' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/appstore.png?format=auto'></img>
                        </div>
                    </div>
                </div>
            </div>
            {/* MOBILE FOOTER  */}
            <div className='h-auto flex flex-col lg:hidden'>
                <div className='h-4/5 relative order-1 lg:order-2 flex justify-between w-full border border-transparent rounded-t-xl bg-[#F58220]'>
                    <div className='px-2 py-5 flex flex-col gap-2'>
                        <h4 className='text-[#101820] text-xl sm:text-2xl font-bold'>ERHA Buddy</h4>
                        <h5 className='text-white font-semibold mb-7 pt-1 text-md sm:text-xl'>Download Sekarang</h5>
                        <img className='mr-4 w-36' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/playstore.png?format=auto'></img>
                        <img className='mr-4 w-36' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/appstore.png?format=auto'></img>
                    </div>
                    <div className='px-2 py-5'>
                        <img className='absolute z-10 left-32 sm:left-64 w-[60%] -top-14 pl-5' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/footer-phone.png?format=auto'></img>
                    </div>
                </div>
                <div className='w-full order-2 z-20 lg:order-1 py-9 h-auto flex flex-col gap-7 items-center bg-[#343434]'>
                    <div className='w-[35%]'>
                        <img src='https://d3sgbq9gctgf5o.cloudfront.net/footers/erha-ultimate.png?format=auto'></img>
                    </div>
                    <div className='w-full'>
                        <h4 className='text-[#F58220] text-center text-[20px] font-bold'>Ikuti Kami</h4>
                        <div className='mt-4'>
                            <ul className='flex gap-1 items-center justify-center'>
                                <li>
                                    <a href='https://www.facebook.com/erhadermatology.official' target='_blank' >
                                        <img className='px-[7px] py-[10px] w-12' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/facebook.png?format=auto'></img>
                                    </a>
                                </li>
                                <li>
                                    <a href='https://www.instagram.com/erha.ultimate/' target='_blank'>
                                        <img className='px-[7px] py-[10px] w-12' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/instagram.png?format=auto'></img>
                                    </a>
                                </li>
                                <li>
                                    <a href='https://www.youtube.com/@dearerha' target='_blank'>
                                        <img className='px-[7px] py-[10px] w-12' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/youtube.png?format=auto'></img>
                                    </a>
                                </li>
                                <li>
                                    <a href='https://www.tiktok.com/@erha.ultimate' target='_blank'>
                                        <img className='px-[7px] py-[10px] w-12' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/tiktok.png?format=auto'></img>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <h4 className='text-[#F58220] text-center text-[20px] font-bold'>Hubungi Kami</h4>
                        <p className='text-white text-[14px] font-bold text-center'>
                            DISTRICT 8 Building Treasury Tower
                            <br />
                            36th & 37th Floor SCBD Lot 28
                            <br />
                            Jl. Jend Sudirman Kav.52 - 53 Jakarta 12190
                        </p>
                        <p className='text-white text-[14px] font-bold text-center flex items-center justify-center mt-4 gap-2'>
                            <img className='w-6 ' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/icon-7.png?format=auto'></img>
                            <a className='hover:text-[#F58220]' href='mailto:dear@erha.co.id'>dear@erha.co.id</a>
                        </p>
                        <p className='text-white text-[14px] font-bold text-center flex items-center justify-center mt-4 gap-2'>
                            <img className='w-6 ' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/icon-5.png?format=auto'></img>
                            <a className='hover:text-[#F58220]' href='tel:02150922121' >(021) 5092 2121</a>
                        </p>
                        <p className='text-white text-[14px] font-bold text-center flex items-center justify-center mt-4 gap-2'>
                            <img className='w-6 ' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/wa-logo.png?format=auto'></img>
                            <a className='hover:text-[#F58220]' href='https://wa.me/6281121212121' target='_blank' >+62-811-2121-2121</a>
                        </p>
                        <h4 className='text-[#F58220] text-center text-[20px] font-bold mt-5 mb-24'>Kebijakan Privasi</h4>

                    </div>

                </div>
            </div>
        </React.Fragment>

    )
}

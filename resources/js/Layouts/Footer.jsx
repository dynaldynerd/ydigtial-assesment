import React from 'react'

export default function Footer() {
    const clickUp = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }
    return (
        <>
            <div onClick={clickUp} className='z-30 fixed bottom-32 lg:bottom-28 right-3 lg:right-8 bg-gray-300 h-auto w-auto rounded-full cursor-pointer'>
                <svg fill='#f58220' xmlns="http://www.w3.org/2000/svg" height="48" viewBox="0 -960 960 960" width="48"><path d="M480-528 296-344l-56-56 240-240 240 240-56 56-184-184Z" /></svg>
            </div>
            <div className='fixed bottom-0 w-full bg-black h-[107px] lg:h-20 rounded-t-3xl z-40'>
                <ul className='flex items-center justify-between text-white px-0 py-8 lg:px-36 lg:py-8'>
                    <li className='flex flex-col justify-center lg:flex-row items-center gap-3 pb-[14.3px]  hover:border-b-8 hover:border-b-orange-600'>
                        <img className='h-[24px] lg:h-[1.3vw]' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/icon-6.png?format=auto'></img>
                        <span className='text-xs font-black lg:text-xl ml-2 lg:ml-0 text-white'>Cari Solusi</span>
                    </li>
                    <li className='flex flex-col lg:flex-row items-center gap-3 pb-[14.3px]  hover:border-b-8 cursor-pointer hover:border-b-orange-600' >
                        <img className='h-[24px] lg:h-[1.3vw]' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/icon-3.png?format=auto'></img>
                        <span className='text-xs font-black lg:text-xl text-white'>Skin Wiki</span>
                    </li>
                    <li className='flex flex-col lg:flex-row items-center gap-3 pb-[14.3px]  hover:border-b-8 cursor-pointer hover:border-b-orange-600' >
                        <img className='h-[24px] lg:h-[1.3vw]' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/icon-4.png?format=auto'></img>
                        <span className='text-xs font-black lg:text-xl text-white'>Buat Jadwal</span>
                    </li>
                    <li className='flex flex-col lg:flex-row items-center gap-3 pb-[14.3px]  hover:border-b-8 cursor-pointer hover:border-b-orange-600' >
                        <img className='h-[24px] lg:h-[1.3vw]' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/icon-8.png?format=auto'></img>
                        <span className='text-xs font-black lg:text-xl text-white'>Cari Klinik & Dokter</span>
                    </li>
                    <li className='flex flex-col lg:flex-row items-center gap-3 pb-[14.3px]  hover:border-b-8 cursor-pointer hover:border-b-orange-600' >
                        <img className='h-[24px] lg:h-[1.3vw]' src='https://d3sgbq9gctgf5o.cloudfront.net/footers/wa-logo.png?format=auto'></img>
                        <span className='text-xs font-black lg:text-xl mr-2 lg:mr-0 text-white'>WA Logo</span>
                    </li>
                </ul>
            </div>
        </>
    )
}

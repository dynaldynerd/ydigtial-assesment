import React from 'react'
import '../../css/app.css'
export default function Header({ children }) {
  const [backgroundColor, setBackgroundColor] = React.useState('rgba(255,255,255)')
  const [searchBarVisible, setSearchBarVisible] = React.useState(false);

  const toggleSearchBar = () => {
    setSearchBarVisible(!searchBarVisible);

  };


  React.useEffect(() => {
    if (searchBarVisible) {
      setBackgroundColor('rgba(255,255,255)');
    } else {
      if (window.scrollY > 10) {
        setBackgroundColor('rgba(255,255,255)');

      } else {

        setBackgroundColor('rgba(255,255,255)');
      }
    }
    const handleScroll = () => {
      if (window.scrollY > 10) {
        setBackgroundColor('rgba(255,255,255)');
      } else {
        setBackgroundColor('rgba(255,255,255)');
      }
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [searchBarVisible])

  const searchBarStyle = {
    display: searchBarVisible ? 'flex' : 'none',
    transform: `translateX(${searchBarVisible ? 0 : '100%'})`,
    transition: 'transform 0.5s ease',
  };
  const [isOpen, setIsOpen] = React.useState(false)
  const [dropdown, setDropdown] = React.useState(false)
  const [dropdownWeb, setDropdownWeb] = React.useState(false)
  return (
    <>
      {/* DRAWER */}
      <div className={
        "fixed overflow-hidden z-50 bg-gray-900 bg-opacity-25 inset-0 transform ease-in-out lg:hidden " +
        (isOpen
          ? " transition-opacity opacity-100 duration-500 translate-x-0  "
          : " transition-all delay-500 opacity-0 translate-x-full  ")
      }>
        <section
          className={
            " w-screen max-w-full right-0 absolute bg-white h-full shadow-xl delay-400 duration-500 ease-in-out transition-all transform  " +
            (isOpen ? " translate-x-0 " : " translate-x-full ")
          }
        >
          <article className="relative w-screen max-w-full pb-10 flex flex-col space-y-6 overflow-y-scroll h-full">
            <div className='flex justify-between items-center py-5 px-[7.5%]'>
              <img className='h-10' src='https://d3sgbq9gctgf5o.cloudfront.net/general/logo.png?format=auto' alt='ERHA Ultimate - Klinik Spesialis Kulit &amp; Rambut'></img>
              <svg onClick={() => {
                setIsOpen(false)
              }} xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><path d="m256-200-56-56 224-224-224-224 56-56 224 224 224-224 56 56-224 224 224 224-56 56-224-224-224 224Z" /></svg>
            </div>
            <div className='w-full h-auto border-2 border-[#f58220]'></div>
            <ul className='w-full px-[7.5%]   flex flex-col gap-3'>
              <li className='font-bold text-base sm:text-xl'>
                <a onClick={() => setDropdown(!dropdown)} href='#' className='flex justify-between items-center'>
                  <span className='hover:text-[#F58220]'>Solusi Sesuai Problem</span>
                  <span className='absolute right-[7.5%]'>
                    <img className={`${dropdown ? 'rotate-180' : ''} w-4 h-auto`} src='https://d3sgbq9gctgf5o.cloudfront.net/footers/arrow-5.png?format=auto'></img>
                  </span>
                </a>
                <ul className={dropdown ? 'w-full px-4 py-3 flex flex-col gap-3' : 'hidden'}>
                  <li>
                    <a href='#' className='hover:text-[#F58220] flex flex-col'>
                      <span>Acne Center</span>
                      <span className='font-normal text-lg'>Kulit Jerawat & Bekas Jerawat</span>
                    </a>
                  </li>
                  <li>
                    <a href='#' className='hover:text-[#F58220] flex flex-col'>
                      <span>Anti Aging Center</span>
                      <span className='font-normal text-lg'>Kulit Kendur & Garis Halus</span>
                    </a>
                  </li>
                  <li>
                    <a href='#' className='hover:text-[#F58220] flex flex-col'>
                      <span>Brightening Center</span>
                      <span className='font-normal text-lg'>Kulit Kusam & Noda Hitam</span>
                    </a>
                  </li>
                  <li>
                    <a href='#' className='hover:text-[#F58220] flex flex-col'>
                      <span>Hair Care Center</span>
                      <span className='font-normal text-lg'>Rambut Rontok & Botak</span>
                    </a>
                  </li>
                  <li>
                    <a href='#' className='hover:text-[#F58220] flex flex-col'>
                      <span>Make Over Center</span>
                      <span className='font-normal text-lg'>Bentuk Wajah & Badan Kurang Ideal</span>
                    </a>
                  </li>
                  <li>
                    <a href='#' className='hover:text-[#F58220] flex flex-col'>
                      <span>Atopy & Skin Disease Center</span>
                      <span className='font-normal text-lg'>Dermatitis Atopik & Penyakit Kulit</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li className='font-bold text-base sm:text-xl'>
                <a className='hover:text-[#F58220]' href='#'>Skin Wiki</a>
              </li>
              <li className='font-bold text-base sm:text-xl'>
                <a className='hover:text-[#F58220]' href='#'>Buat Jadwal Ke Klinik</a>
              </li>
              <li className='font-bold text-base sm:text-xl'>
                <a className='hover:text-[#F58220]' href='#'>Cari Klinik</a>
              </li>
              <li className='font-bold text-base sm:text-xl'>
                <a className='hover:text-[#F58220]' href='#'>Promo Spesial</a>
              </li>
              <li className='font-bold text-base sm:text-xl'>
                <a className='hover:text-[#F58220]' href='#'>Kisah ERHA</a>
              </li>
              <li className='font-bold text-base sm:text-xl'>
                <a className='hover:text-[#F58220]' href='#'>Info Terkini</a>
              </li>
              <li className='font-bold text-base sm:text-xl'>
                <a className='hover:text-[#F58220]' href='#'>Hubungi Kami</a>
              </li>
            </ul>
          </article>
        </section>
        {/* <section
          className=" w-screen h-full cursor-pointer "
          onClick={() => {
            setIsOpen(false);
          }}
        ></section> */}
      </div >

      {/* NAVBAR  */}
      <div className='w-full z-20 fixed flex justify-between p-2 lg:py-[15px] lg:px-28 items-center shadow-md' style={{ backgroundColor }}>
        <img className='h-10' src='https://d3sgbq9gctgf5o.cloudfront.net/general/logo.png?format=auto' alt='ERHA Ultimate - Klinik Spesialis Kulit &amp; Rambut'></img>
        <ul className='lg:flex gap-2 hidden'>
          <li className='cursor-pointer  '>
            <a href='#' onClick={() => setDropdownWeb(!dropdownWeb)} className='hover:text-[#F58220] flex justify-between items-center'>
              <span className=''> Solusi Sesuai Problem</span>
              <svg className={dropdownWeb ? 'rotate-180' : ''} xmlns="http://www.w3.org/2000/svg" height="24" fill='#F58220' viewBox="0 -960 960 960" width="24"><path d="M480-345 240-585l56-56 184 184 184-184 56 56-240 240Z" /></svg>

            </a>
            <div className={dropdownWeb ? 'block' : 'hidden'}>
              <div className='flex items-start bg-white border rounded-b-xl flex-col left-[40%] mt-[1.5vw] min-w-[20vw] absolute z-20 txs'>
                <a href='#' className='cursor-pointer hover:bg-[#FFFAF4] border-b border-b-[#ddd] flex flex-col px-[1.35vw] py-[1vw] w-full '>
                  <p className='text-[#F58220] font-extrabold'>Acne Center</p>
                  <p>Kulit Jerawat & Bekas Jerawat</p>
                </a>
                <a href='#' className='cursor-pointer hover:bg-[#FFFAF4] border-b border-b-[#ddd] flex flex-col px-[1.35vw] py-[1vw] w-full '>
                  <p className='text-[#F58220] font-extrabold'>Anti Aging Center</p>
                  <p>Kulit Jerawat & Bekas Jerawat</p>
                </a>
                <a href='#' className='cursor-pointer hover:bg-[#FFFAF4] border-b border-b-[#ddd] flex flex-col px-[1.35vw] py-[1vw] w-full '>
                  <p className='text-[#F58220] font-extrabold'>Brightening Center</p>
                  <p>Kulit Kusam & Noda Hitam</p>
                </a>
                <a href='#' className='cursor-pointer hover:bg-[#FFFAF4] border-b border-b-[#ddd] flex flex-col px-[1.35vw] py-[1vw] w-full '>
                  <p className='text-[#F58220] font-extrabold'>Hair Care Center</p>
                  <p>Rambut Rontok & Botak</p>
                </a>
                <a href='#' className='cursor-pointer hover:bg-[#FFFAF4] border-b border-b-[#ddd] flex flex-col px-[1.35vw] py-[1vw] w-full '>
                  <p className='text-[#F58220] font-extrabold'>Make Over Center</p>
                  <p>Bentuk Wajah & Badan Kurang Ideal</p>
                </a>
                <a href='#' className='cursor-pointer hover:bg-[#FFFAF4] border-b border-b-[#ddd] flex flex-col px-[1.35vw] py-[1vw] w-full '>
                  <p className='text-[#F58220] font-extrabold'>Atopy & Skin Disease Center</p>
                  <p>Dermatitis Atopik & Penyakit Kulit</p>
                </a>

              </div>
            </div>
          </li>
          <li className='hover:text-[#F58220] cursor-pointer'>Promo Spesial</li>
          <li className='hover:text-[#F58220] cursor-pointer'>Kisah ERHA</li>
          <li className='hover:text-[#F58220] cursor-pointer'>Info Terkini</li>
        </ul>
        <div className='flex items-center gap-3'>
          <div className='w-[calc(100%-24vw)] items-center flex gap-2 h-full absolute right-[23vw] bg-white top-0' style={searchBarStyle}>
            <span onClick={toggleSearchBar}>
              <svg xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 -960 960 960" width="18"><path d="m256-200-56-56 224-224-224-224 56-56 224 224 224-224 56 56-224 224 224 224-56 56-224-224-224 224Z" /></svg>
            </span>
            <form className='w-full'>
              <input className='input-mobile-search placeholder:p-0' placeholder='Pencarian' type='text'></input>
              {/* <div className='border-2 w-full border-gray-400'></div> */}
            </form>
          </div>
          <svg className={searchBarVisible ? 'bg-orange-300 text-white rounded-full' : ''} fill={searchBarVisible ? '#FFFFFF' : ''} onClick={searchBarVisible ? null : toggleSearchBar} xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z" /></svg>
          <svg onClick={() => setIsOpen(true)} className='lg:hidden' xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><path d="M120-240v-80h720v80H120Zm0-200v-80h720v80H120Zm0-200v-80h720v80H120Z" /></svg>
          <svg className='hidden lg:block cursor-pointer' xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><path d="M480-480q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47ZM160-160v-112q0-34 17.5-62.5T224-378q62-31 126-46.5T480-440q66 0 130 15.5T736-378q29 15 46.5 43.5T800-272v112H160Zm80-80h480v-32q0-11-5.5-20T700-306q-54-27-109-40.5T480-360q-56 0-111 13.5T260-306q-9 5-14.5 14t-5.5 20v32Zm240-320q33 0 56.5-23.5T560-640q0-33-23.5-56.5T480-720q-33 0-56.5 23.5T400-640q0 33 23.5 56.5T480-560Zm0-80Zm0 400Z" /></svg>
        </div>
      </div>

    </>
  )
}

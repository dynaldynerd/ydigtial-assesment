import React from 'react'
import Footer from '@/Layouts/Footer'
import Header from '@/Layouts/Header'
import FooterBanner from '@/Layouts/FooterBanner'
import { Head } from '@inertiajs/react'
import { usePage } from '@inertiajs/react'
import axios from 'axios'
import star from '../../../public/icons8-star-48.png'


export default function DoctorDetails() {
  const { id, data } = usePage().props

  const [defaultValue, setDefaultValue] = React.useState(data?.kliniks[0]?.klinik_id)
  const selectValue = (e) => {
    const { value } = e.target
    setDefaultValue(value)
  }

  const filteredSchedules = data.schedules?.filter(items => items.klinik_id === parseInt(defaultValue));
  console.log(filteredSchedules)
  React.useEffect(() => {
  }, [defaultValue])
  //   const [data, setData] = React.useState(null)
  return (
    <>
      <Head title='Doctor Detail' />
      <div className='h-auto w-full'>
        <Header />
        <div className='h-auto'>
          {/* bg-gradient-to-tr from-amber-300 via-orange-300 to-orange-500 */}

          <div className='relative h-40  bg-[#fffaf4]'>
            <div className='flex flex-col lg:flex-row lg:justify-between p-2 pt-[114px] ml-2 mr-2 lg:ml-28 lg:mr-28'>
              <img className='h-28 w-28 border-2 border-white rounded-full object-cover' src={data?.url_img}></img>
              <div className='pt-2 lg:pt-20 flex gap-3 items-center'>
                <button className='px-2 py-1 rounded-md border border-gray-300 font-bold'>:</button>
                <button className='px-2 py-1 rounded-md border border-gray-300 font-bold'>Message</button>
                <button className='px-2 py-1 rounded-md border border-gray-300 font-bold bg-orange-300 hover:bg-orange-400 text-white '>Book a Session</button>
              </div>
            </div>
            <div className='ml-2 mr-2 lg:ml-32 lg:mr-28 flex flex-col gap-3 lg:flex-row my-3'>
              {/* LEFT SIDE  */}
              <div className='w-full lg:w-3/4'>
                <p className='font-bold text-2xl'>{data?.nama}</p>
                <p>No STR {data?.no_izin_praktek}</p>
                <ul className='flex flex-wrap lg:flex-row gap-1 items-center'>
                  <li className='border-r text-md border-r-gray-100 pr-0.5 lg:pr-4'>@{data.twitter}</li>
                  <li className='border-r flex items-center text-md border-r-gray-100 pr-0.5 lg:pr-4'>
                    <svg xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 -960 960 960" width="24"><path d="M480-480q33 0 56.5-23.5T560-560q0-33-23.5-56.5T480-640q-33 0-56.5 23.5T400-560q0 33 23.5 56.5T480-480Zm0 294q122-112 181-203.5T720-552q0-109-69.5-178.5T480-800q-101 0-170.5 69.5T240-552q0 71 59 162.5T480-186Zm0 106Q319-217 239.5-334.5T160-552q0-150 96.5-239T480-880q127 0 223.5 89T800-552q0 100-79.5 217.5T480-80Zm0-480Z" /></svg>
                    <span>{data?.lokasi}</span>
                  </li>
                  <li className='border-r flex items-center text-md border-r-gray-100 pr-0.5 lg:pr-4'>
                    <svg xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 -960 960 960" width="24"><path d="M200-80q-33 0-56.5-23.5T120-160v-560q0-33 23.5-56.5T200-800h40v-80h80v80h320v-80h80v80h40q33 0 56.5 23.5T840-720v560q0 33-23.5 56.5T760-80H200Zm0-80h560v-400H200v400Zm0-480h560v-80H200v80Zm0 0v-80 80Zm280 240q-17 0-28.5-11.5T440-440q0-17 11.5-28.5T480-480q17 0 28.5 11.5T520-440q0 17-11.5 28.5T480-400Zm-160 0q-17 0-28.5-11.5T280-440q0-17 11.5-28.5T320-480q17 0 28.5 11.5T360-440q0 17-11.5 28.5T320-400Zm320 0q-17 0-28.5-11.5T600-440q0-17 11.5-28.5T640-480q17 0 28.5 11.5T680-440q0 17-11.5 28.5T640-400ZM480-240q-17 0-28.5-11.5T440-280q0-17 11.5-28.5T480-320q17 0 28.5 11.5T520-280q0 17-11.5 28.5T480-240Zm-160 0q-17 0-28.5-11.5T280-280q0-17 11.5-28.5T320-320q17 0 28.5 11.5T360-280q0 17-11.5 28.5T320-240Zm320 0q-17 0-28.5-11.5T600-280q0-17 11.5-28.5T640-320q17 0 28.5 11.5T680-280q0 17-11.5 28.5T640-240Z" /></svg>
                    <span>{data?.tanggal_gabung}</span>
                  </li>
                  <li className='text-sm flex items-center gap-1'>
                    <img className='w-5 h-5' src={star} />
                    <span className='text-lg align-middle text-center'>{data?.rating} ({data?.sessi})</span>
                  </li>
                </ul>
              </div>
              {/* RIGHT SIDE  */}
              <div className='w-full lg:w-1/4 flex gap-3'>
                <button className='p-2 w-1/2 cursor-default rounded-md bg-[#fffaf4]'>
                  <p className='xl:text-2xl text-center font-bold text-black'>{data?.sessi}</p>
                  <p className='xl:text-sm font-semibold text-black'>Booked Session</p>
                </button>
                <button className='p-2 w-1/2 cursor-default rounded-md bg-[#fffaf4]'>
                  <p className='xl:text-2xl text-center font-bold text-black'>{data?.pengalaman}+</p>
                  <p className='xl:text-sm font-semibold text-black'>Years Experience</p>
                </button>
                {/* <button className='p-2 w-1/2 cursor-default flex flex-col border border-gray-300 rounded-md bg-amber-300'>
                <p className='xl:text-2xl text-center font-bold text-orange-700'>5</p>
                <p className='xl:text-sm font-semibold text-orange-700'>Years Experience</p>
              </button> */}
              </div>
            </div>
            <div className='border ml-2 mr-2 lg:ml-32 lg:mr-28  border-gray-200'></div>
            <div className='ml-3 mr-3 lg:ml-32 lg:mr-28 flex flex-col lg:flex-row my-2'>
              <div className='w-full lg:w-3/4'>
                <div className='mb-[50px]'>
                  <h1 className='font-bold text-xl mt-3 lg:mt-0'>Clinic Interest</h1>
                  <p>{data?.interest}</p>
                </div>
                <div>
                  <h1 className='font-bold text-xl'>Pendidikan</h1>
                  <p>{data?.pendidikan.name} : {data?.pendidikan?.degree}</p>
                </div>
              </div>
              <div className='w-full lg:w-1/4 mb-32'>
                <h1 className='font-bold text-xl'>Lokasi Klinik</h1>
                <select onChange={selectValue} className='mt-3 p-3 border border-orange-300 rounded-full w-full' defaultValue={defaultValue}>
                  {data?.kliniks?.map((items, index) => (
                    <option key={index} value={items.klinik_id}>{items.nama}</option>
                  ))}
                </select>
                <table className='border border-gray-300 w-full mt-5 rounded-md'>
                  <thead className='hidden'>
                    <tr>
                      <th>Day</th>
                      <th>Hours</th>
                    </tr>
                  </thead>
                  <tbody >
                    {filteredSchedules.map((filteredItems, index) => (
                      <tr key={index} className='border-b border-gray-300'>
                        <td className='p-4'>{filteredItems.nama_schedule}</td>
                        <td className='text-[#F58220]'>{filteredItems.description}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
            <FooterBanner />
          </div>

        </div>

        <Footer />
      </div>
    </>
  )
}

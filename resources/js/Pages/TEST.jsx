import { usePage } from '@inertiajs/react'
import React from 'react'

export default function TEST() {
    const { id } = usePage().props
    return (
        <div>{id ? id : 0}</div>
    )
}

import Footer from '@/Layouts/Footer'
import Header from '@/Layouts/Header'
import React from 'react'
import '../../css/app.css'
import FooterBanner from '@/Layouts/FooterBanner'
import { Head, usePage } from '@inertiajs/react'
import axios from 'axios'

export default function ClinicDetails() {
    const { id, data } = usePage().props
    const [active, setActive] = React.useState(false)
    const [currentIndex, setCurrentIndex] = React.useState(0);

    const handlePrevious = () => {
        setCurrentIndex((prevIndex) => (prevIndex === 0 ? images.length - 1 : prevIndex - 1));
    };

    const handleNext = () => {
        setCurrentIndex((prevIndex) => (prevIndex === images.length - 1 ? 0 : prevIndex + 1));
    };

    React.useEffect(() => {
        const interval = setInterval(() => {
            setCurrentIndex((prevIndex) => (prevIndex === images.length - 1 ? 0 : prevIndex + 1));
        }, 3000);

        return () => clearInterval(interval);
    }, []);

    const images = data.galeri.map(items => items.url)
    return (
        <>
            <Head title='Clinic Detail' />
            <div className='h-auto w-full'>
                <Header />
                <div className='h-auto relative py-10'>
                    <div className="relative w-full max-w-full mx-auto h-full">
                        <button className="absolute left-3 top-1/2 transform -translate-y-1/2 px-3 py-1 bg-gray-800 text-white rounded-full" onClick={handlePrevious}>
                            Previous
                        </button>
                        <button className="absolute right-3 top-1/2 transform -translate-y-1/2 px-3 py-1 bg-gray-800 text-white rounded-full" onClick={handleNext}>
                            Next
                        </button>
                        <div className='flex overflow-hidden'>
                            {images.map((image, index) => (
                                <img
                                    key={index}
                                    src={image}
                                    alt={`Slider ${index}`}
                                    className={`w-full h-96 object-cover ${index === currentIndex ? '' : 'hidden'}`}
                                />

                            ))}
                        </div>
                        {/* <div className="flex overflow-hidden rounded-lg relative">
                            {images.map((image, index) => (
                                <img
                                    key={index}
                                    src={image}
                                    alt={`Slider ${index}`}
                                    className={`w-full absolute top-0 left-0 transition duration-500 ease-in-out ${index === currentIndex ? 'opacity-100' : 'opacity-0'
                                        }`}
                                    style={{ zIndex: index === currentIndex ? 1 : 0 }}
                                />
                            ))}
                        </div> */}

                    </div>

                    {/* <img className='h-[26rem] w-full object-cover' src='https://erha-prod.s3.ap-southeast-1.amazonaws.com/erhav2/clinic_details/20230216065407-0.jpeg'></img> */}
                    <div className='bg-[#fffaf4] w-full flex flex-col justify-center items-center py-5'>
                        <h4 className='mb-5 mt-8 font-bold text-xl '>{data.nama}</h4>
                        <p className='text-gray-400 text-center font-medium text-lg mb-4'>{data.surat_izin}</p>
                        <p className='font-medium text-center text-lg mb-4'>{data.alamat}</p>
                        <a target='_blank' href={`http://maps.google.com/maps?z=12&t=m&q=loc:${data.long}+${data.lat}`} className='underline text-orange-500 font-semibold text-xl'>Lihat Peta</a>
                    </div>
                    <div className='w-full p-2 lg:px-28 mb-32 '>
                        {/* MOBILE  */}
                        <div className='flex flex-col justify-center items-center'>

                            <div className='w-[100vw] flex items-center justify-center h-12 md:hidden mt-8'>
                                <ul className='flex items-center justify-between w-2/3 md:w-1/3 mb-12 bg-[#f28020] px-1 py-2 rounded-full'>
                                    <li onClick={() => setActive(false)} className={active
                                        ? 'h-10 flex items-center justify-center text-white rounded-full w-1/2 px-2 font-bold text-lg'
                                        : 'bg-white h-10 flex items-center justify-center text-black rounded-full w-1/2 px-2 font-bold text-lg'}>Informasi</li>
                                    <li onClick={() => setActive(true)} className={!active
                                        ? 'h-10 flex items-center justify-center text-white rounded-full w-1/2 px-2 font-bold text-lg'
                                        : 'bg-white h-10 flex items-center justify-center text-black rounded-full w-1/2 px-2 font-bold text-lg'}>Dokter</li>
                                </ul>
                            </div>
                            <div className={`${active ? 'hidden' : ''} md:hidden flex flex-col items-start justify-center`}>
                                <h4 className='font-bold text-xl'>Informasi Kontak</h4>
                                <div className='w-full flex items-center gap-3 mb-2'>
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" className="w-5 h-6" viewBox="0 0 16 16">
                                        <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"></path>
                                    </svg>
                                    <a className='text-[#f5821e] underline text-base' href='https://wa.me/6281219115991' target='_blank'>+{data.wa}</a>
                                </div>
                                <div className='w-full flex items-center gap-3 mb-2'>
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="w-5 h-6" viewBox="0 0 16 16">
                                        <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"></path>
                                    </svg>
                                    <a className='text-[#f5821e] underline text-base' href='tel:6281286947211"' target='_blank'>{data.no_telp}</a>
                                </div>
                                <div className=''>
                                    <h4 className='font-bold text-xl'>Nama Apoteker</h4>
                                    <p className='font-bold'>{data?.apoteker?.nama}</p>
                                    <p className='text-gray-400 mb-4'>{data?.apoteker?.no_izin_praktek}</p>
                                    <p className='mb-4'>{data?.apoteker?.schedules[0]?.nama_schedule} - {data?.apoteker?.schedules[0]?.description}</p>
                                    <p className='mb-4'>{data?.apoteker?.schedules[1]?.nama_schedule} - {data?.apoteker?.schedules[1]?.description}</p>
                                </div>
                                <div className=''>
                                    <h4 className='font-bold text-xl mb-2'>Waktu Kerja</h4>
                                    <p className=''>{data?.schedule?.nama} - {data?.schedule?.description}</p>
                                </div>
                            </div>
                        </div>
                        <div className={`${!active ? 'hidden' : ''} flex flex-wrap gap-3`}>
                            {data?.doctors?.map(item => (
                                <div className='bg-[#FFFAF4] relative w-full lg:w-full h-auto border border-none rounded-lg'>
                                    <div className='flex gap-3'>
                                        <img className='h-auto object-cover w-48 h-48 rounded-lg' src={item?.url_img}></img>
                                        <div className='px-[5px] py-[0.75vw]'>
                                            <h5 className='text-base'>{item?.nama}</h5>
                                            <p className='line-clamp-1'>{item?.interest}</p>
                                            <a href={route('doctor.details', { id: item.id })} className='absolute bottom-3 underline text-orange-500 '>Lihat Detail</a>
                                        </div>
                                    </div>
                                </div>
                            ))}


                        </div>

                        <div className='grid grid-cols-6'>

                            {/* WEB  */}
                            <div className='col-span-6 hidden md:block md:col-span-3'>
                                <div className='flex flex-col'>
                                    <div className=''>
                                        <h4 className='font-bold text-xl mb-3'>Nama Apoteker</h4>
                                        <p className='font-bold'>{data?.apoteker?.nama}</p>
                                        <p className='text-gray-400 mb-4'>{data?.apoteker?.no_izin_praktek}</p>
                                        <p className='mb-4'>{data?.apoteker?.schedules[0]?.nama_schedule} - {data?.apoteker?.schedules[0]?.description}</p>
                                        <p className='mb-4'>{data?.apoteker?.schedules[1]?.nama_schedule} - {data?.apoteker?.schedules[1]?.description}</p>
                                    </div>
                                    <div className=''>
                                        <h4 className='font-bold text-xl mb-2'>Waktu Kerja</h4>
                                        <p className=''>{data?.schedule?.nama} - {data?.schedule?.description}</p>
                                    </div>
                                </div>
                            </div>

                            <div className='col-span-6 hidden md:block md:col-span-3'>
                                <div className="flex flex-col">
                                    <h4 className='font-bold text-xl mb-3'>Dokter</h4>
                                    <div className='flex flex-wrap gap-3'>
                                        {data?.doctors?.map(item => (
                                            <div className='bg-[#FFFAF4] relative w-full lg:w-full h-auto border border-none rounded-lg'>
                                                <div className='flex gap-3'>
                                                    <img className='h-auto object-cover w-48 rounded-lg' src={item?.url_img}></img>
                                                    <div className='px-[5px] py-[0.75vw]'>
                                                        <h5 className='text-base'>{item?.nama}</h5>
                                                        <p className='line-clamp-1'>{item?.interest}</p>
                                                        <a href={route('doctor.details', { id: item.id })} className='absolute bottom-3 underline text-orange-500 '>Lihat Detail</a>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <FooterBanner />
                </div>
                <Footer />
            </div>
        </>
    )
}

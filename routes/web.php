<?php

use App\Http\Controllers\ProfileController;
use App\Models\Doctor;
use App\Models\Klinik;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/test/{id}', function ($id) {

    return Inertia::render('TEST', ['id' => $id]);
});

Route::get('/clinics/detail/{id}', function ($id) {
    $klinik = Klinik::with([
        'apoteker' => function ($query) {
            $query->with('schedules:id,apoteker_id,nama as nama_schedule,description');
        },
        'doctors' => function ($query) {
            $query->select('doctors.id', 'nama', 'interest', 'url_img');
        },
        'galeri:id,name,url,klinik_id',
        'schedule:id,nama,description,klinik_id'
    ])->select(
        'id',
        'nama',
        'alamat',
        'no_telp',
        'wa',
        'kota',
        'daerah',
        'surat_izin',
        'long',
        'lat'
    )->findOrFail($id);
    return Inertia::render('ClinicDetails', ['id' => $id, 'data' => $klinik]);
})->name('clinics.detail');


Route::get('/doctors/details/{id}', function ($id) {
    $doctor = Doctor::with([
        'schedules' => function ($query) {
            $query->select('id', 'doctor_id', 'klinik_id', 'nama as nama_schedule', 'description');
        },
        'pendidikan' => function ($query) {
            $query->select('id', 'doctor_id', 'name', 'degree');
        },
        'kliniks' => function ($query) {
            $query->select('kliniks.id', 'klinik_id', 'doctor_id', 'nama');
        }
    ])
        ->select(
            'id',
            'nama',
            'pengalaman',
            'no_izin_praktek',
            'twitter',
            'lokasi',
            'tanggal_gabung',
            'rating',
            'sessi',
            'interest',
            'url_img',
            'created_at',
            'updated_at',
            'deleted_at'
        )
        ->findOrFail($id);
    return Inertia::render('DoctorDetails', ['id' => $id, 'data' => $doctor]);
})->name('doctor.details');

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';

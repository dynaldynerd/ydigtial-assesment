<?php

namespace App\Http\Controllers;

use App\Models\Klinik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class KlinikController extends Controller
{
    public function show($id)
    {
        try {
            $klinik = Klinik::with([
                'apoteker' => function ($query) {
                    $query->with('schedules:id,apoteker_id,nama as nama_schedule,description');
                },
                'doctors' => function ($query) {
                    $query->select('doctors.id', 'nama', 'interest', 'url_img');
                },
                'galeri:id,name,url,klinik_id',
                'schedule:id,nama,description,klinik_id'
            ])->select(
                'id',
                'nama',
                'alamat',
                'no_telp',
                'wa',
                'kota',
                'daerah',
                'surat_izin',
                'long',
                'lat'
            )->findOrFail($id);

            return response()->json($klinik);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return response()->json(['error' => $th->getMessage()], 500);
        }
    }
}

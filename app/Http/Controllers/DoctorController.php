<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DoctorController extends Controller
{
    public function getDoctorSchedule($id)
    {
        try {
            $doctor = Doctor::with([
                'schedules' => function ($query) {
                    $query->select('id', 'doctor_id', 'klinik_id', 'nama as nama_schedule', 'description');
                },
                'pendidikan' => function ($query) {
                    $query->select('id', 'doctor_id', 'name', 'degree');
                },
                'kliniks' => function ($query) {
                    $query->select('kliniks.id', 'klinik_id', 'doctor_id', 'nama');
                }
            ])
                ->select(
                    'id',
                    'nama',
                    'pengalaman',
                    'no_izin_praktek',
                    'twitter',
                    'lokasi',
                    'tanggal_gabung',
                    'rating',
                    'sessi',
                    'interest',
                    'created_at',
                    'updated_at',
                    'deleted_at'
                )
                ->findOrFail($id);

            return response()->json($doctor);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return response()->json(['error' => $th->getMessage()], 500);
        }
    }
}

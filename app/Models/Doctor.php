<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;
    protected $table = 'doctors';
    public function kliniks()
    {
        return $this->belongsToMany(Klinik::class, 'kliniks_doctors');
    }
    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'doctor_id');
    }
    public function pendidikan()
    {
        return $this->hasOne(Pendidikan::class, 'doctor_id');
    }
}

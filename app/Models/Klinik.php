<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Klinik extends Model
{
    use HasFactory;
    protected $table = 'kliniks';
    public function doctors()
    {
        return $this->belongsToMany(Doctor::class, 'kliniks_doctors');
    }
    public function galeri()
    {
        return $this->hasMany(Galeri::class, 'klinik_id');
    }
    public function apoteker()
    {
        return $this->hasOne(Apoteker::class, 'klinik_id');
    }
    public function schedule()
    {
        return $this->hasOne(Schedule::class, 'klinik_id');
    }
}

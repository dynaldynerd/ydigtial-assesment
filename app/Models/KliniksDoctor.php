<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KliniksDoctor extends Model
{
    use HasFactory;
    protected $table = 'kliniks_doctors';
    public function kliniks()
    {
        return $this->belongsToMany(Klinik::class, 'klinik_id');
    }
}

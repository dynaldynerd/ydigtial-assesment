<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apoteker extends Model
{
    use HasFactory;
    protected $table = 'apotekers';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'apoteker_id');
    }
    public function klinik()
    {
        return $this->belongsTo(Klinik::class);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->integer('pengalaman');
            $table->string('no_izin_praktek');
            $table->string('twitter');
            $table->string('lokasi');
            $table->string('tanggal_gabung');
            $table->integer('rating');
            $table->integer('sessi');
            $table->string('url_img');
            $table->string('interest');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('doctors');
    }
};

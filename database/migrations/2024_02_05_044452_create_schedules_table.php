<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('description');
            $table->unsignedBigInteger('klinik_id')->nullable();
            $table->unsignedBigInteger('apoteker_id')->nullable();
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->foreign('klinik_id')->references('id')->on('kliniks')->onDelete('cascade');
            $table->foreign('apoteker_id')->references('id')->on('apotekers')->onDelete('cascade');
            $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schedules');
    }
};

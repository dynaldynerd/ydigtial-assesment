<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Apoteker;
use App\Models\Doctor;
use App\Models\Galeri;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use App\Models\Klinik;
use App\Models\KliniksDoctor;
use App\Models\Pendidikan;
use App\Models\Schedule;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $jsonPathKlinik = database_path("/seeders/json/klinik.json");
        $jsonDataKlinik = json_decode(File::get($jsonPathKlinik), true);
        $jsonPathApoteker = database_path("/seeders/json/apoteker.json");
        $jsonDataApoteker = json_decode(File::get($jsonPathApoteker), true);
        $jsonPathDokter = database_path("/seeders/json/dokter.json");
        $jsonDataDokter = json_decode(File::get($jsonPathDokter), true);
        $jsonPathGaleri = database_path("/seeders/json/galeri.json");
        $jsonDataGaleri = json_decode(File::get($jsonPathGaleri), true);
        $jsonPathPendidikan = database_path("/seeders/json/pendidikan.json");
        $jsonDataPendidikan = json_decode(File::get($jsonPathPendidikan), true);
        $jsonPathSchedule = database_path("/seeders/json/schedule.json");
        $jsonDataSchedule = json_decode(File::get($jsonPathSchedule), true);
        $jsonPathKlinikDoctor = database_path("/seeders/json/klinik_doctor.json");
        $jsonDataKlinikDoctor = json_decode(File::get($jsonPathKlinikDoctor), true);

        foreach ($jsonDataKlinik as $klinik) {
            Klinik::factory()->create([
                'id' => $klinik['id'],
                'nama' => $klinik['nama'],
                'alamat' => $klinik['alamat'],
                'no_telp' => $klinik['no_telp'],
                'wa' => $klinik['wa'],
                'kota' => $klinik['kota'],
                'daerah' => '-',
                'surat_izin' => $klinik['surat_izin'],
                'long' => $klinik['long'],
                'lat' => $klinik['lat'],
            ]);
        }

        foreach ($jsonDataApoteker as $apoteker) {
            Apoteker::factory()->create([
                'id' => $apoteker['id'],
                'nama' => $apoteker['nama'],
                'no_izin_praktek' => $apoteker['no_izin_praktek'],
                'klinik_id' => $apoteker['klinik_id'],
            ]);
        }

        foreach ($jsonDataDokter as $dokter) {
            Doctor::factory()->create([
                'id' => $dokter['id'],
                'nama' => $dokter['nama'],
                'pengalaman' => $dokter['pengalaman'],
                'no_izin_praktek' => $dokter['no_izin_praktek'],
                'twitter' => $dokter['twitter'],
                'lokasi' => $dokter['lokasi'],
                'tanggal_gabung' => $dokter['tanggal_gabung'],
                'rating' => $dokter['rating'],
                'sessi' => $dokter['sessi'],
                'interest' => $dokter['interest'],
                'url_img' => $dokter['url_image'],
            ]);
        }

        foreach ($jsonDataGaleri as $galeri) {
            Galeri::factory()->create([
                'id' => $galeri['id'],
                'name' => $galeri['name'],
                'url' => $galeri['url'],
                'klinik_id' => $galeri['klinik_id'],
            ]);
        }

        foreach ($jsonDataKlinikDoctor as $klinik_doctor) {
            KliniksDoctor::factory()->create([
                'id' => $klinik_doctor['id'],
                'klinik_id' => $klinik_doctor['klinik_id'],
                'doctor_id' => $klinik_doctor['doctor_id'],
            ]);
        }
        foreach ($jsonDataPendidikan as $pendidikan) {
            Pendidikan::factory()->create([
                'id' => $pendidikan['id'],
                'name' => $pendidikan['name'],
                'degree' => $pendidikan['degree'],
                'doctor_id' => $pendidikan['doctor_id'],
            ]);
        }
        foreach ($jsonDataSchedule as $schedule) {
            Schedule::factory()->create([
                'id' => $schedule['id'],
                'nama' => $schedule['nama'],
                'description' => $schedule['description'],
                'klinik_id' => $schedule['klinik_id'],
                'apoteker_id' => $schedule['apoteker_id'],
                'doctor_id' => $schedule['doctor_id'],
            ]);
        }

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
